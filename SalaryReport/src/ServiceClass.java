
public class ServiceClass implements ServiceInterface {
	public boolean checkSalary(long salary) {
		if (salary >= 1)
			return true;
		else
			return false;
	}

	public boolean checkIncrement(float increment) {
		if (increment > 0)
			return true;
		else
			return false;
	}

	public boolean checkDecrement(float decrement) {
		if (decrement > 0)
			return true;
		else
			return false;
	}

	public boolean justCheck(int xyz) {
		if (xyz >= 1)
			return true;
		else
			return false;
	}
}
