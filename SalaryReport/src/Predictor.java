
public class Predictor {
	long salary;
	float increment;
	int frequency;
	float decrement;
	int deduction;
	int years;

	public long getSalary() {
		return salary;
	}

	public void setSalary(long salary) {
		this.salary = salary;
	}

	public float getIncrement() {
		return increment;
	}

	public void setIncrement(float increment) {
		this.increment = increment;
	}

	public int getFrequency() {
		return frequency;
	}

	public void setFrequency(int frequency) {
		this.frequency = frequency;
	}

	public float getDecrement() {
		return decrement;
	}

	public void setDecrement(float decrement) {
		this.decrement = decrement;
	}

	public int getDeduction() {
		return deduction;
	}

	public void setDeduction(int deduction) {
		this.deduction = deduction;
	}

	public int getYears() {
		return years;
	}

	public void setYears(int years) {
		this.years = years;
	}

	public void incrementReport() {
		int j = getYears();
		long sal = getSalary();
		long sal1;
		long amount = 0;
		long amount1 = 0;
		System.out.println("INCREMENT REPORT");
		System.out.println("______________________________________________________________________________");
		System.out.println("YEARS | STARING SALARY | NUMBER OF INCREMENTS | INCREMENT% | INCREMENT AMOUNT");
		for (int i = 0; i < j; i++) {
			sal = sal + amount1;
			sal1 = sal;
			int freq = getFrequency();
			while ((freq--) > 0) {
				amount = (long) ((sal1 * getIncrement()) / 100);
				sal1 = sal1 + amount;
				// amount1=amount1+amount;
				amount1 = sal1 - sal;
			}
			System.out.println(i + 1 + "            " + sal + "              " + getFrequency() + "                  "
					+ getIncrement() + "%            " + amount1);
		}
		System.out.println("______________________________________________________________________________");
		System.out.println();
	}

	public void decrementReport() {
		int j = getYears();
		long sal = getSalary();
		long sal1;
		long amount = 0;
		long amount1 = 0;
		System.out.println("DECREMENT REPORT");
		System.out.println("______________________________________________________________________________");
		System.out.println("YEARS | STARING SALARY | NUMBER OF DECREMENTS | DECREMENT% | DECREMENT AMOUNT");
		for (int i = 0; i < j; i++) {
			sal = sal - amount1;
			sal1 = sal;
			int dedu = getDeduction();
			while ((dedu--) > 0) {
				amount = (long) ((sal1 * getDecrement()) / 100);
				sal1 = sal1 - amount;
				// amount1=amount1+amount;
				amount1 = sal - sal1;
			}
			System.out.println(i + 1 + "            " + sal + "              " + getDeduction() + "                  "
					+ getDecrement() + "%            " + amount1);

		}
		System.out.println("______________________________________________________________________________");
		System.out.println();

	}

	public void finalReport() {
		int j = getYears();
		long sal = getSalary();
		long sal1, sal2;
		long amount1 = 0;
		long amount2 = 0;
		long amount3 = 0;
		long amount4 = 0;
		long amount = 0;
		System.out.println("PREDICTION");
		System.out.println("______________________________________________________________________________");
		System.out.println("YEARS | STARING SALARY | INCREMENT AMOUNT | DECREMENT AMOUNT | SALARY GROWTH");
		for (int i = 0; i < j; i++) {
			sal = sal + amount;
			sal1 = sal;
			int freq = getFrequency();
			int dedu = getDeduction();
			while ((freq--) > 0) {
				amount1 = (long) ((sal1 * getIncrement()) / 100);
				sal1 = sal1 + amount1;
				amount2 = sal1 - sal;
			}

			sal2 = sal1;
			while ((dedu--) > 0) {
				amount3 = (long) ((sal2 * getDecrement()) / 100);
				sal2 = sal2 - amount3;
				amount4 = sal1 - sal2;
			}
			amount = amount2 - amount4;

			System.out.println(i + 1 + "            " + sal + "              " + amount2 + "                  "
					+ amount4 + "             " + amount);

		}
		System.out.println("______________________________________________________________________________");
		System.out.println();

	}
}
