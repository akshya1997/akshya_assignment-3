import java.util.Scanner;

class Display {
	public static void displayOptions() {
		System.out.println("For Quarterly--->4");
		System.out.println("For Half-yearly--->2");
		System.out.println("For Annually--->1");
	}
}

public class Main {

	public static void main(String[] args) {

		// Initialize objects
		ServiceInterface service = new ServiceClass();
		Predictor pd = new Predictor();
		Scanner sc = new Scanner(System.in);

		// Initialize variables
		long salary;
		float increment;
		int frequency;
		float decrement;
		int deduction;
		int years;

		// check salary
		while (true) {
			System.out.println("Enter your salary");
			salary = sc.nextLong();
			boolean isValid = service.checkSalary(salary);
			if (isValid) {
				break;
			} else {
				System.out.println("WARNING: Salary should be more than 1");
			}
		}

		// check increment
		while (true) {
			System.out.println("Enter increment percent");
			increment = sc.nextFloat();
			boolean isValid = service.checkIncrement(increment);
			if (isValid) {
				break;
			} else {
				System.out.println("WARNING: Increment should be positive number");
			}
		}

		// check increment frequency
		while (true) {
			System.out.println("How frequently is increment received?");
			Display.displayOptions();
			frequency = sc.nextInt();
			boolean isValid = service.justCheck(frequency);
			if (isValid) {
				break;
			} else {
				System.out.println("WARNING: Enter more than one number");
			}
		}

		// check decrement
		while (true) {
			System.out.println("Enter decrement percent");
			decrement = sc.nextFloat();
			boolean isValid = service.checkDecrement(decrement);
			if (isValid) {
				break;
			} else {
				System.out.println("WARNING: Decrement should be positive number");
			}
		}

		// check decrement frequency
		while (true) {
			System.out.println("How frequently is deduction done");
			Display.displayOptions();
			deduction = sc.nextInt();
			boolean isValid = service.justCheck(deduction);
			if (isValid) {
				break;
			} else {
				System.out.println("WARNING: Enter more than one number");
			}
		}

		System.out.println("Prediction of your salary in years");
		years = sc.nextInt();

		// calling the predictor
		pd.setSalary(salary);
		pd.setIncrement(increment);
		pd.setFrequency(frequency);
		pd.setDecrement(decrement);
		pd.setDeduction(deduction);
		pd.setYears(years);

		// the display operations
		pd.incrementReport();
		pd.decrementReport();
		pd.finalReport();

	}

}
