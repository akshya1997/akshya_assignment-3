
public interface ServiceInterface {
	boolean checkSalary(long salary);

	boolean checkIncrement(float increment);

	boolean checkDecrement(float decrement);

	boolean justCheck(int xyz);
}
